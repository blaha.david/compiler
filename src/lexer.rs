use std::mem;
use serde::{Serialize, Deserialize};

pub struct Lexer<'t> {
    input: &'t str,
    tokens: Vec<Token>,
    current: Token,

    escape_char: bool,
    comment: bool,

    line_number: usize,
    line_column: usize,
}

impl<'t> Lexer<'t> {
    pub fn new (input: &'t str) -> Self {
        Self {
            input,

            tokens: Vec::new(),
            current: Token {
                token_type: TokenType::Empty,
                inner_text: String::new(),
                line_number: 0,
                line_column: 0
            },

            escape_char: false,
            comment: false,

            line_number: 1,
            line_column: 1,
        }
    }
    pub fn tokenize(&mut self) -> Vec<Token> {

        for ch in self.input.chars() {
            if self.comment {
                if ch == '\n' {
                    self.comment = false;
                }
                continue;
            }

            match ch {
                '0'..='9' => {
                    if self.current.token_type == TokenType::Empty {
                        self.begin_token(TokenType::IntLiteral);
                    }
                    self.current.inner_text.push(ch);
                }

                '\n' => {
                    if !self.escape_char {
                        // Can escape at the end of the line to continue on the next line
                        self.end_token();
                    }
                    self.escape_char = false;
                    self.line_number += 1;
                    self.line_column = 1;
                    continue;
                }

                '{' | '}' | '(' | ')' | '=' | '+' | '-' | '*' | '/' | ';' | '[' | ']' => {
                    if self.current.token_type == TokenType::StrLiteral || self.current.token_type == TokenType::CharLiteral
                    {
                        self.current.inner_text.push(ch);
                    } else {
                        self.end_token();
                        self.begin_token(TokenType::Operator);
                        self.current.inner_text.push(ch);
                        self.end_token();
                    }
                }

                ' ' | '\t' => {
                    if self.current.token_type != TokenType::Empty {
                        if self.current.token_type == TokenType::StrLiteral
                            || self.current.token_type == TokenType::CharLiteral
                        {
                            self.current.inner_text.push(ch);
                        } else {
                            self.end_token();
                        }
                    }
                }

                '"' => {
                    if self.current.token_type == TokenType::StrLiteral {
                        if self.escape_char {
                            self.current.inner_text.push('"');
                            self.escape_char = false;
                        } else {
                            self.end_token();
                        }
                    } else if self.current.token_type == TokenType::CharLiteral {
                        self.current.inner_text.push(ch);
                    } else {
                        self.end_token();
                        self.begin_token(TokenType::StrLiteral);
                    }
                }

                '\'' => {
                    if self.current.token_type == TokenType::CharLiteral {
                        if self.escape_char {
                            self.current.inner_text.push(ch);
                            self.escape_char = false;
                        } else {
                            self.end_token();
                        }
                    } else if self.current.token_type == TokenType::StrLiteral {
                        self.current.inner_text.push(ch);
                    } else {
                        self.end_token();
                        self.begin_token(TokenType::CharLiteral);
                    }
                }

                '\\' => {
                    if self.escape_char {
                        self.current.inner_text.push('\\');
                        self.escape_char = false;
                    } else {
                        self.escape_char = true;
                    }
                }

                '#' => {
                    if self.current.token_type == TokenType::StrLiteral
                        || self.current.token_type == TokenType::CharLiteral
                    {
                        self.current.inner_text.push(ch);
                    } else {
                        self.comment = true;
                    }
                }

                _ => {
                    if self.current.token_type == TokenType::IntLiteral {
                        self.end_token();
                        self.begin_token(TokenType::Identifier);
                    } else if self.current.token_type == TokenType::Empty {
                        self.begin_token(TokenType::Identifier);
                    }
                    self.current.inner_text.push(ch);
                }
            }

            self.line_column += 1;
        }

        self.end_token();

        return mem::take(&mut self.tokens);
    }

    fn begin_token(&mut self, token_type: TokenType) {
        self.current.line_number = self.line_number;
        self.current.line_column = self.line_column;

        self.current.token_type = token_type;
    }

    fn end_token(&mut self) {
        // helper function
        if self.current.token_type == TokenType::Empty {
            return;
        };

        self.tokens.push(self.current.clone());

        self.begin_token(TokenType::Empty);
        self.current.inner_text.clear();
    }
}

#[derive(PartialEq, Eq, Copy, Clone, Hash, Serialize, Deserialize)]
pub enum TokenType {
    Empty = 0,
    IntLiteral,
    CharLiteral,
    StrLiteral,
    Identifier,
    Operator,
}

#[derive(PartialEq, Eq, Clone, Hash, Serialize, Deserialize)]
pub struct Token {
    pub token_type: TokenType,
    pub inner_text: String,
    pub line_number: usize,
    pub line_column: usize,

}

impl std::fmt::Display for Token {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}({})", self.token_type, self.inner_text)
    }
}
impl std::fmt::Debug for Token {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}({}):{}:{}", self.token_type, self.inner_text, self.line_number, self.line_column)
    }
}

impl std::fmt::Display for TokenType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let token_str: [&str; 6] = [
            "Empty",
            "IntLiteral",
            "CharLiteral",
            "StrLiteral",
            "Identifier",
            "Operator",
        ];

        write!(f, "{}", token_str[*self as i32 as usize])
    }
}




#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn tokenize_variable_declaration() {
        let program = r#"int a;"#;

        let tokens = Lexer::new(&program).tokenize();
        assert_eq!(tokens, vec![
                   Token{token_type: TokenType::Identifier, inner_text: "int".to_string(), line_number: 1, line_column: 1},
                   Token{token_type: TokenType::Identifier, inner_text: "a".to_string(), line_number: 1, line_column: 5},
                   Token{token_type: TokenType::Operator, inner_text: ";".to_string(), line_number: 1, line_column: 6},
        ]);

    }

    #[test]
    fn tokenize_variable_assignment() {
        let program = r#"a = 1;"#;

        let tokens = Lexer::new(&program).tokenize();
        assert_eq!(tokens, vec![
               Token{token_type: TokenType::Identifier, inner_text: "a".to_string(), line_number: 1, line_column: 1},
               Token{token_type: TokenType::Operator, inner_text: "=".to_string(), line_number: 1, line_column: 3},
               Token{token_type: TokenType::IntLiteral, inner_text: "1".to_string(), line_number: 1, line_column: 5},
               Token{token_type: TokenType::Operator, inner_text: ";".to_string(), line_number: 1, line_column: 6},
        ]);
    }

    #[test]
    fn tokenize_basic_math() {
        let program = r#"b = a + 1;"#;

        let tokens = Lexer::new(&program).tokenize();
        assert_eq!(tokens, vec![
               Token{token_type: TokenType::Identifier, inner_text: "b".to_string(), line_number: 1, line_column: 1},
               Token{token_type: TokenType::Operator, inner_text: "=".to_string(), line_number: 1, line_column: 3},
               Token{token_type: TokenType::Identifier, inner_text: "a".to_string(), line_number: 1, line_column: 5},
               Token{token_type: TokenType::Operator, inner_text: "+".to_string(), line_number: 1, line_column: 7},
               Token{token_type: TokenType::IntLiteral, inner_text: "1".to_string(), line_number: 1, line_column: 9},
               Token{token_type: TokenType::Operator, inner_text: ";".to_string(), line_number: 1, line_column: 10},
        ]);

    }
}
