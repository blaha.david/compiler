use lazy_static::lazy_static;
use crate::lexer::{Token, TokenType};
use std::collections::HashMap;
use serde::{Serialize, Deserialize};


lazy_static! {
    static ref TYPES: HashMap<&'static str, Type> = HashMap::from([
        ("void", Type {builtin_type: BuiltinType::Void}),
        ("int", Type {builtin_type: BuiltinType::Int}),
        ("char", Type {builtin_type: BuiltinType::Char}),
        //("str", Type {builtin_type: BuiltinType::Str}),
    ]);
    static ref OPERATORS: HashMap<&'static str, Operator> = HashMap::from([
        ("+", Operator::Add),
        ("-", Operator::Sub),
        ("*", Operator::Mul),
        ("/", Operator::Div),
    ]);
}


#[derive(PartialEq, Eq, Clone, Debug, Serialize, Deserialize)]
pub struct AST {
    pub statements: Vec<Statement>
}
#[derive(PartialEq, Eq, Clone, Debug, Serialize, Deserialize)]
pub struct Variable {
    pub name: String
}

#[derive(PartialEq, Eq, Clone, Serialize, Deserialize)]
//#[serde(tag="type",content="value")]
pub enum Statement {
    VariableDeclaration(VariableDeclaration),
    Assignment(Assignment),
}

#[derive(PartialEq, Eq, Clone, Serialize, Deserialize)]
//#[serde(tag="statement")]
pub struct VariableDeclaration {
    pub data_type: BuiltinType,
    pub name: String
}

#[derive(PartialEq, Eq, Clone, Serialize, Deserialize)]
//#[serde(tag="statement")]
pub struct Assignment {
    pub left: Variable,
    pub right: Expression
}

#[derive(PartialEq, Eq, Clone, Serialize, Deserialize)]
#[serde(tag="type",content="value")]
pub enum Literal {
    IntLiteral(i32),
    StrLiteral(String)
}

#[derive(PartialEq, Eq, Clone, Serialize, Deserialize)]
#[serde(tag="expression",content="value")]
pub enum Expression {
    Literal(Literal),
    Variable(Variable),
    Operation(Operation)
}

#[derive(PartialEq, Eq, Clone, Serialize, Deserialize)]
pub struct Operation {
    pub operator: Operator,
    pub left: Box<Expression>,
    pub right: Box<Expression>
}

#[derive(PartialEq, Eq, Copy, Clone, Serialize, Deserialize)]
pub enum Operator {
    Add,
    Sub,
    Mul,
    Div,
}


// --------------------------------------------------------------------------------
// PARSER:
// --------------------------------------------------------------------------------

pub struct Parser<'p> {
    index: usize,
    tokens: &'p Vec<Token>
}

impl<'p> Parser<'p> {
    pub fn new(tokens: &'p Vec<Token>) -> Self {
        Self {
            index: 0,
            tokens,
        }
    }
    pub fn parse(&mut self) -> AST {
        let mut statements: Vec<Statement> = Vec::new();
        while self.index < self.tokens.len() {
            // variable declaration
            let var_declaration_opt = self.expect_variable_declaration();
            if var_declaration_opt.is_some() {
                let var_declaration = var_declaration_opt.unwrap();
                statements.push(Statement::VariableDeclaration(var_declaration));
                continue;
            }

            // variable assignment
            let assignment_opt = self.expect_assignment();
            if assignment_opt.is_some() {
                let assignment = assignment_opt.unwrap();
                statements.push(Statement::Assignment(assignment));
                continue;
            }

            let token = &self.tokens[self.index];
            panic!("Unexpected token '{}' at {}:{}", token.inner_text, token.line_number, token.line_column);
        }
        return AST{statements};
    }
    fn expect_variable_declaration (&mut self) -> Option<VariableDeclaration> {
        let start = self.index;

        // expect Type
        let var_type_opt = self.expect_type();
        if var_type_opt.is_none() {
            self.index = start;
            return None
        }
        let var_type = var_type_opt.unwrap().builtin_type;

        // expect name
        let var_name_opt = self.expect_identifier("");
        if var_name_opt.is_none() {
            self.index = start;
            return None
        }
        let var_name = var_name_opt.unwrap().inner_text.to_owned();

        // expect semicolon
        let semicolon = self.expect_operator(";");
        if semicolon.is_none() {
            self.index = start;
            return None
        }

        return Some(VariableDeclaration{
            data_type: var_type,
            name: var_name,
        });
    }

    fn expect_expression(&mut self, prefer_var: bool) -> Option<Expression> {
        if self.index >= self.tokens.len() {
            return None;
        }

        let start = self.index;

        if prefer_var {
            // try variable
            let var_opt = self.expect_identifier("");
            if var_opt.is_some() {
                return Some(Expression::Variable(Variable{name: var_opt.unwrap().inner_text.to_owned()}));
            }
        }

        // try literal
        let literal_opt = self.expect_literal();
        if literal_opt.is_some() {
            let literal_token = literal_opt.unwrap();
            return match literal_token.token_type {
                TokenType::StrLiteral => Some(Expression::Literal(Literal::StrLiteral(literal_token.inner_text.to_owned()))),
                TokenType::IntLiteral => Some(Expression::Literal(Literal::IntLiteral(literal_token.inner_text.parse().unwrap()))),
                _ => panic!()
            };
        }

        // expect operation
        let operation_opt = self.expect_operation();
        if operation_opt.is_some() {
            return Some(Expression::Operation(operation_opt.unwrap()));
        }

        // try variable
        let var_opt = self.expect_identifier("");
        if var_opt.is_some() {
            return Some(Expression::Variable(Variable{name: var_opt.unwrap().inner_text.to_owned()}));
        }

        self.index = start;
        None
    }

    fn expect_operation (&mut self) -> Option<Operation> {
        if self.index >= self.tokens.len() {
            return None;
        }
        let start = self.index;

        // expect expression
        let left_opt = self.expect_expression(true);
        if left_opt.is_none() {
            self.index = start;
            return None
        }
        let left = left_opt.unwrap();

        // expect operator
        let mut operator: Option<Operator> = None;
        for (ch, op) in OPERATORS.iter() {
            let try_operator = self.expect_operator(ch);
            if try_operator.is_some() {
                operator = Some(*op);
                break;
            }
        }
        if operator.is_none() {
            self.index = start;
            return None
        }

        // expect expression
        let right_opt = self.expect_expression(true);
        if right_opt.is_none() {
            self.index = start;
            return None
        }
        let right = right_opt.unwrap();

        return Some(Operation{
            operator: operator.unwrap(),
            left: Box::new(left),
            right: Box::new(right)
        });
    }

    fn expect_assignment(&mut self) -> Option<Assignment> {
        let start = self.index;

        // expect name
        let var_name_opt = self.expect_identifier("");
        if var_name_opt.is_none() {
            self.index = start;
            return None
        }
        let var_name = var_name_opt.unwrap().inner_text.to_owned();

        // expect equals
        let equals = self.expect_operator("=");
        if equals.is_none() {
            self.index = start;
            return None
        }

        // expect expression
        let expression_opt = self.expect_expression(false);
        if expression_opt.is_none() {
            self.index = start;
            return None
        }
        let expression = expression_opt.unwrap();

        // expect semicolon
        let semicolon = self.expect_operator(";");
        if semicolon.is_none() {
            self.index = start;
            return None
        }

        return Some(Assignment{
            left: Variable{name: var_name},
            right: expression,
        });
    }


    // --------------------------------------------------------------------------------
    // Token parsing:
    // --------------------------------------------------------------------------------

    fn expect_identifier(&mut self, name: &str) -> Option<&Token> {
        let token;
        match self.tokens.get(self.index) {
            Some(val) => token = val,
            None => return None,
        }

        if token.token_type != TokenType::Identifier {
            return None;
        }
        if !name.is_empty() && name != token.inner_text {
            return None;
        }

        self.index += 1;
        return Some(token);
    }


    fn expect_operator(&mut self, name: &str) -> Option<&Token> {
        let token;
        match self.tokens.get(self.index) {
            Some(val) => token = val,
            None => return None,
        }

        if token.token_type != TokenType::Operator {
            return None;
        }
        if !name.is_empty() && name != token.inner_text {
            return None;
        }

        self.index += 1;
        return Some(token);
    }
    fn expect_literal(&mut self) -> Option<&Token> {
        let token;
        match self.tokens.get(self.index) {
            Some(val) => token = val,
            None => return None,
        }

        if token.token_type != TokenType::IntLiteral && token.token_type != TokenType::StrLiteral {
            return None;
        }

        self.index += 1;
        return Some(token);

        //if token.token_type == TokenType::StrLiteral { return Some(token); }
        //if token.token_type == TokenType::IntLiteral { return Some(token); }

        //return None;
    }
    fn expect_type(&mut self) -> Option<&Type> {
        let possible_type = self.expect_identifier("");

        if possible_type.is_none() {
            return None;
        }

        return TYPES.get(&possible_type.unwrap().inner_text.as_str());
    }
}


// --------------------------------------------------------------------------------
// Types:
// --------------------------------------------------------------------------------

#[derive(PartialEq, Eq, Copy, Clone, Serialize, Deserialize)]
struct Type {
    builtin_type: BuiltinType,
    //pointer: bool,
    //structure: bool,
    //fields: Vec<Type>, // used for structs, and a single field is used for pointer
}

#[derive(PartialEq, Eq, Copy, Clone, Serialize, Deserialize)]
pub enum BuiltinType {
    Void,
    Int,
    Char,
    Str,
}

impl std::fmt::Display for BuiltinType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let type_str: [&str; 4] = ["Void", "Int", "Char", "Str"];

        write!(f, "{}", type_str[*self as i32 as usize])
    }
}

impl std::fmt::Debug for Statement {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", serde_json::to_string(self).unwrap())
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_variable_declaration() {
        let tokens = vec![
                   Token{token_type: TokenType::Identifier, inner_text: "int".to_string(), line_number: 1, line_column: 1},
                   Token{token_type: TokenType::Identifier, inner_text: "a".to_string(), line_number: 1, line_column: 5},
                   Token{token_type: TokenType::Operator, inner_text: ";".to_string(), line_number: 1, line_column: 6},
        ];

        let ast = Parser::new(&tokens).parse();
        assert_eq!(ast, AST{statements: vec![
            Statement::VariableDeclaration(VariableDeclaration{data_type: BuiltinType::Int, name: "a".to_string()}),
        ]});
    }

    #[test]
    fn parse_variable_assignment() {
        let tokens = vec![
               Token{token_type: TokenType::Identifier, inner_text: "a".to_string(), line_number: 1, line_column: 1},
               Token{token_type: TokenType::Operator, inner_text: "=".to_string(), line_number: 1, line_column: 3},
               Token{token_type: TokenType::IntLiteral, inner_text: "1".to_string(), line_number: 1, line_column: 5},
               Token{token_type: TokenType::Operator, inner_text: ";".to_string(), line_number: 1, line_column: 6},
        ];

        let ast = Parser::new(&tokens).parse();
        assert_eq!(ast, AST{statements: vec![
                   Statement::Assignment(Assignment{left: Variable{name: "a".to_string()}, right: Expression::Literal(Literal::IntLiteral(1))}),
        ]});
    }

    #[test]
    fn parse_basic_math() {
        let tokens = vec![
               Token{token_type: TokenType::Identifier, inner_text: "b".to_string(), line_number: 1, line_column: 1},
               Token{token_type: TokenType::Operator, inner_text: "=".to_string(), line_number: 1, line_column: 3},
               Token{token_type: TokenType::Identifier, inner_text: "a".to_string(), line_number: 1, line_column: 5},
               Token{token_type: TokenType::Operator, inner_text: "+".to_string(), line_number: 1, line_column: 7},
               Token{token_type: TokenType::IntLiteral, inner_text: "1".to_string(), line_number: 1, line_column: 9},
               Token{token_type: TokenType::Operator, inner_text: ";".to_string(), line_number: 1, line_column: 10},
        ];
        let ast = Parser::new(&tokens).parse();
        assert_eq!(ast, AST{statements: vec![
                   Statement::Assignment(Assignment{
                       left: Variable{name: "b".to_string()},
                       right: Expression::Operation(Operation{
                           operator: Operator::Add,
                           left: Box::new(Expression::Variable(Variable{name: "a".to_string()})),
                           right: Box::new(Expression::Literal(Literal::IntLiteral(1)))
                       })
                   }),
        ]});
    }

    #[test]
    #[should_panic(expected="Unexpected token 'lmao' at 2:1")]
    fn unexpected_token() {
        let tokens = vec![
                   Token{token_type: TokenType::Identifier, inner_text: "int".to_string(), line_number: 1, line_column: 1},
                   Token{token_type: TokenType::Identifier, inner_text: "a".to_string(), line_number: 1, line_column: 5},
                   Token{token_type: TokenType::Operator, inner_text: ";".to_string(), line_number: 1, line_column: 6},
                   Token{token_type: TokenType::Identifier, inner_text: "lmao".to_string(), line_number: 2, line_column: 1},
                   Token{token_type: TokenType::Identifier, inner_text: "yeet".to_string(), line_number: 2, line_column: 5},
        ];
        Parser::new(&tokens).parse();
    }
}
