mod lexer;
mod parser;
mod compiler;

use std::env;
use std::fs::File;
use std::io::Read;
use serde_json;

fn main() -> std::io::Result<()> {

    let args: Vec<String> = env::args().collect();

    if args.len() <= 1 {
        println!("Usage: {} <file>", args[0]);
        return Ok(());
    }

    let mut f = File::open(&args[1])?;

    let mut input: String = String::new();
    f.read_to_string(&mut input)?;

    let tokens = lexer::Lexer::new(&input).tokenize();

    //for token in &tokens {
        //println!("{}({})", token.token_type, token.inner_text);
    //}

    let statements = parser::Parser::new(&tokens).parse();

    //let json = serde_json::to_string(&statements).unwrap();
    //println!("{}", json);

    let assembly = compiler::Compiler::new(&statements).compile();

    for line in assembly {
        println!("{}", line);
    }

    Ok(())
}




