use std::collections::HashMap;
use std::mem;
use crate::parser::*;

#[derive(PartialEq, Eq, Clone)]
pub struct Variable {
    pub data_type: BuiltinType,
    pub name: String,
}

pub struct Compiler<'c> {
    ast: &'c AST,

    variables: HashMap<String,Variable>,
    instructions: Vec<String>,
}

impl<'c> Compiler<'c> {
    pub fn new(ast: &'c AST) -> Self {
        Self {
            ast,
            variables: HashMap::new(),
            instructions: Vec::new(),
        }
    }

    pub fn compile (&mut self) -> Vec<String> {

        for statement in self.ast.statements.iter() {
            match statement {
                Statement::VariableDeclaration(var) => {
                    self.variables.insert(var.name.to_owned(), Variable { data_type: var.data_type, name: var.name.to_owned()});
                }
                Statement::Assignment(assignment) => {
                    let var = &assignment.left;
                    if !self.variables.contains_key(&var.name) {
                        panic!("Variable '{}' was not declared", var.name)
                    }

                    match &assignment.right {
                        Expression::Literal(literal) => {
                            match literal {
                                Literal::IntLiteral(int) => {
                                    self.instructions.push(format!("seta {}", int));
                                },
                                Literal::StrLiteral(str) => {
                                    // TODO: put string value into data section
                                    let var_name = format!("var{}", self.variables.len());
                                    self.variables.insert(var_name.clone(), Variable{data_type: BuiltinType::Str, name: var_name.clone()});
                                    self.instructions.push(format!("mova ${}", var_name));
                                }
                            }
                        },
                        Expression::Variable(var) => {
                            self.instructions.push(format!("mova ${}", var.name));
                        },
                        Expression::Operation(operation) => {
                            // TODO: chain expressions
                            self.evaluate_expression(&operation.left);
                            self.evaluate_expression(&operation.right);
                        }
                    }

                    self.instructions.push(format!("stra ${}", var.name));
                }
            }
        }

        let mut assembly: Vec<String> = Vec::new();

        assembly.push(".entry _start".to_string());
        assembly.push(".section code".to_string());
        assembly.push("_start:".to_string());
        for ins in &self.instructions {
            assembly.push(format!("    {}", ins.to_string()));
        }

        assembly.push("".to_string());
        assembly.push(".section data".to_string());
        for var in self.variables.values() {
            assembly.push(format!("    {}: {}", var.name, 0))
        }

        return assembly;
    }

    fn evaluate_expression (&mut self, expression: &Expression) {
        match expression {
            Expression::Literal(literal) => {

            },
            Expression::Variable(var) => {

            },
            Expression::Operation(operation) => {
                self.evaluate_expression(&operation.left);
                self.evaluate_expression(&operation.right);
            }
        }
    }

}


#[cfg(test)]
mod tests {
    use crate::compiler;
    use crate::parser::*;

    #[test]
    fn compile() {
        let ast = AST{statements: vec![
            Statement::VariableDeclaration(VariableDeclaration{data_type: BuiltinType::Int, name: "a".to_string()}),
            Statement::VariableDeclaration(VariableDeclaration{data_type: BuiltinType::Int, name: "b".to_string()}),
            Statement::Assignment(Assignment{
                left: Variable{name: "b".to_string()},
                right: Expression::Operation(Operation{
                    operator: Operator::Add,
                    left: Box::new(Expression::Variable(Variable{name: "a".to_string()})),
                    right: Box::new(Expression::Literal(Literal::IntLiteral(1)))
                })
            }),
        ]};

        compiler::Compiler::new(&ast).compile();
    }
}
